#include "console.h"
#include "font.h"
#include "kprintf.h"
#include "disk.h"
#include "bgd.h"
#include "superblock.h"
//#include "inode.h"
#include "direntry.h"
#include "filesystem.h"

//for putc
int cc = 0;
int cr = 0;

union U{
	//static char block[4096];
	struct BlockGroupDescriptor bgd[1];
};

//static char bigBoiBuff[4096];
//lab 8
//static union U mBGDT;
volatile unsigned char * framebuffer;
struct MultibootInfo * pixelMbi;
//struct Superblock sprBlk;
//lab 9

void kmemcpy(void * dv, const void * s, unsigned n){
	char * d = (char *)dv;
	char * k = (char *)s;
	while(n--){
		*d++ = *k++;
	}
}

void console_init(struct MultibootInfo * mbi)
{
	framebuffer = (volatile unsigned char *)(unsigned)mbi->mbiFramebufferAddress;
	pixelMbi = mbi;

	//--------------Lab 9!!!!
	//disk_read_block(4, (void *)bigBoiBuff);
	//ino = (struct Inode *)bigBoiBuff;

	//disk_read_block(ino[1].direct[0], bigBoiBuff);
	//kprintf("%.*s",);
	//list_directory(2, 0);
	//------------ lab 8 -------------------
	// disk_read_block(0, (void *)bigBoiBuff);
	// kprintf("Volume Label: ");
	// kmemcpy(&sprBlk, (bigBoiBuff + 1024), 1024);
	// //We do length of the name and then character pointer.
	// kprintf("%.*s", 16, sprBlk.volname);
	// kprintf("   Free Blocks: %d\n", sprBlk.free_block_count);
	// kprintf("Blocks per group: %d  Total Blocks: %d\n", sprBlk.blocks_per_group, sprBlk.block_count);
	// //disk_read_sector(2, bigBoiBuff);
	
	// kprintf("Reading BGDT from group 0\n");
	// //Block 1 in group 0
	// disk_read_block(1, mBGDT.block);
	// int i;
	// for(i = 0; i < 3; i++){
	// 	kprintf("Group %d: Free blocks = %d\n", i, mBGDT.bgd[i].free_blocks);
	// }

	// //Block 1 in Group 1
	// kprintf("Reading BGDT from group 1\n");
	// disk_read_block(32769, mBGDT.block);
	// for(i = 0; i < 3; i++){
	// 	kprintf("Group %d: Free blocks = %d\n", i, mBGDT.bgd[i].free_blocks);
	// }

	// //Block 1 in Group 2
	// kprintf("Reading BGDT from group 2\n");
	// disk_read_block(65537, mBGDT.block);
	// for(i = 0; i < 3; i++){
	// 	kprintf("Group %d: Free blocks = %d\n", i, mBGDT.bgd[i].free_blocks);
	// }

}

void set_pixel(int x, int y, int r, int g, int b)
{
	r >>= (8 - pixelMbi->mbiFramebufferRedMask);
	b >>= (8 - pixelMbi->mbiFramebufferBlueMask);
	g >>= (8 - pixelMbi->mbiFramebufferGreenMask);
	unsigned short ColorVal =  (r << pixelMbi->mbiFramebufferRedPos) | (g << pixelMbi->mbiFramebufferGreenPos) | (b << pixelMbi->mbiFramebufferBluePos);
	framebuffer[(2 * x) + (pixelMbi->mbiFramebufferPitch * y)] = ColorVal; 
	framebuffer[((2 * x) + (pixelMbi->mbiFramebufferPitch * y)) + 1] = ColorVal >> 8; //The first half of color val is in the second part

}

void drawR(int Start, int Stop, int intermediate /*int r, int g, int b*/)
{
	int x;
	int y;

	for (y = Start; y < Stop; y ++){
			set_pixel(Start, y, 255, 255, 0);
	}
	for (x = Start; x < intermediate; x ++){
		set_pixel(x, Start, 255, 255, 0);
	}
	for (y = Start; y < intermediate; y ++){
		set_pixel(intermediate, y, 255, 255, 0);
	}
	for (x = Start; x < intermediate; x ++){
		set_pixel(x, intermediate, 255, 255, 0);
	}

	int x_one = Start, y_one = intermediate;
	int x_two = intermediate, y_two = Stop;
	int dx = x_two - x_one;
	int dy = y_two - y_one;
	for (x = Start; x < intermediate; x ++){
		y = y_one + dy * (x - x_one) / dx;
		set_pixel(x, y, 255, 255, 0);
	}
}

void console_draw_char(int x, int y, char ch){
	const int * C = font_data[(int)ch];
	int cy, cx;
	for(cy = 0; cy < CHAR_HEIGHT; cy++){
		for(cx = 0; cx < CHAR_WIDTH; cx++){
			if((MASK_VALUE >> cx ) & C[cy])
				set_pixel(cx+x, cy+y, 255, 39, 126);
			else
				set_pixel(cx+x, cy+y, 0,0,0);
		}
	}
}

void console_draw_string(int x, int y, const char * ch){

	int count = 0;
 	while(ch[count] != '\0'){
 		console_draw_char(x, y, ch[count]);
 		x += CHAR_WIDTH; 
 		count++;
 	}
}

//Drawing lines of text
void kmemset(void * p, char c, int n){
	char * v = (char *)p;
	int i;
	for(i = 0; i < n; i++){
		v[i] = c;
	}
}

void console_putc(char c){
	//debugging purposes
	outb(0x3f8, c);
	int entireScreen = pixelMbi->mbiFramebufferWidth * (pixelMbi->mbiFramebufferHeight * 2);

	if(c == '\n'){
		cc = 0;
		cr++;
	}
	else if(c == '\r'){
		cc = 0;
	}
	else if(c == '\f'){
		kmemset((void*)framebuffer, 0, entireScreen);
		cc = 0;
		cr = 0;
	}
	else if(c == '\t'){
		if(cc > pixelMbi->mbiFramebufferWidth / CHAR_WIDTH - 7){
			cr++;
			cc = 0;
		}
		else{
			do {
				cc++;
			} while (cc%8 != 0);
		}
		//goes to the next column that is divisble by 8.
		//We can check to see which column that is by moduling our current col by 8.
		//if that operation = 0
		//we have a special case if cc is already div by 8, we can use a do while to 
		//shove cc into the next %8 col.
	}
	else if(c == '\x7f'){
		if(cc == 0){
			if (cr > 0){
				cr--;
				cc = pixelMbi->mbiFramebufferWidth / CHAR_WIDTH -1;
			}
		} else {
			cc--;
		}
		console_draw_char(cc * CHAR_WIDTH, cr * CHAR_HEIGHT, ' ');//we could set this char to null
	}
	else if(c == '\b'){
		if(cc == 0){
			if(cr > 0){
				cr--;
				cc = pixelMbi->mbiFramebufferWidth / CHAR_WIDTH;
			}
		} else {
			cc--;
		}
	}
	else{
		console_draw_char(cc * CHAR_WIDTH, cr * CHAR_HEIGHT, c);
		cc++;
		if(cc >= pixelMbi->mbiFramebufferWidth / CHAR_WIDTH){
			cc = 0;
			cr++;
			if(cr > pixelMbi->mbiFramebufferHeight / CHAR_HEIGHT - 2){ //-2 
				cr = pixelMbi->mbiFramebufferHeight / CHAR_HEIGHT-2;
				kmemcpy((char*)framebuffer, 
					(char*)framebuffer + CHAR_HEIGHT * pixelMbi->mbiFramebufferPitch, 
					(pixelMbi->mbiFramebufferHeight - CHAR_HEIGHT) * pixelMbi->mbiFramebufferPitch);

				kmemset((char*)framebuffer + (pixelMbi->mbiFramebufferHeight - CHAR_HEIGHT) * pixelMbi->mbiFramebufferPitch, 
					0, 
					CHAR_HEIGHT * pixelMbi->mbiFramebufferPitch);
			}
		}
	}
}