#include "interrupt.h"
#include "kprintf.h"
#include "filesystem.h"
#include "errno.h"

struct GDTEntry gdt[] = {
	{ 0, 0, 0, 0, 0, 0 },    //zeros
	{  0xffff, 0,0,0, 0xcf92, 0 },	//data, ring 0
	{  0xffff, 0,0,0, 0xcf9a, 0 },	//code, ring 0
	{  0xffff, 0,0,0, 0xcff2, 0 },   //data, ring 3
	{  0xffff, 0,0,0, 0xcffa, 0 },   //code, ring 3
	{ 0, 0, 0, 0, 0, 0 }            //task selector
};
// Some globals
unsigned char kernelStack[450];

unsigned ring0StackInfo[] = {
	0,
	(unsigned)(kernelStack+sizeof(kernelStack)),
	1<<3 //low 3 bits are flag bits
};

void haltForever(void){
	while(1){
		asm volatile("hlt" ::: "memory");
	}
}

__attribute__((__interrupt__))
void unknownInterrupt(struct InterruptFrame * fr){
	kprintf("\nFatal exception at eip=%x\n",fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void unknownInterruptWithCode(struct InterruptFrame * fr, unsigned code){
	kprintf("Fatal exception: Code=%x eip=%x\n",code,fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void divideByZeroInterrupt(struct InterruptFrame * fr){
	kprintf("\nDivide by Zero at eip=%x\n",fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void debugInterrupt(struct InterruptFrame * fr){
	kprintf("Debug exception: eip=%x\n",fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void protectionFaultInterrupt(struct InterruptFrame * fr, unsigned code){
	kprintf("Protection Fault exception: Code = %x eip=%x\n",code,fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void pageFaultInterrupt(struct InterruptFrame * fr, unsigned code){
	kprintf("Page Fault exception: Code = %x eip=%x\n",code,fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void badOpcodeInterrupt(struct InterruptFrame * fr, unsigned code){
	kprintf("Bad Opcode exception: Code=%x eip=%x\n",code,fr->eip);
	haltForever();
}

struct IDTentry idt[32];
void table(int i, void * func){
	unsigned x = (unsigned)func;
	idt[i].addrLow = x&0xffff;
	idt[i].selector = 2 << 3;
	idt[i].zero = 0;
	idt[i].flags = 0x8e;
	idt[i].addrHigh = x >> 16;
}

//interrupt initialization
void interrupt_init(){

	unsigned tmp = (unsigned) ring0StackInfo;
	gdt[5].limitLow = sizeof(ring0StackInfo);
	gdt[5].base0 = tmp & 0xff;
	gdt[5].base1 = (tmp>>8) & 0xff;
	gdt[5].base2 = (tmp>>16) & 0xff;
	gdt[5].flagsAndLimitHigh = 0x00e9;
	gdt[5].base3 = (tmp>>24) & 0xff;

	struct LGDT lgdt;
	lgdt.size = sizeof(gdt);
	lgdt.addr = &gdt[0];
	asm volatile( "lgdt [eax]\n"
				"ltr bx"
				:  //no outputs
				: "a"(&lgdt),	//put address of gdt in eax
				  "b"( (5<<3) | 3)	//put task register index in ebx
				: "memory" );
	
	struct LIDT ltmp;
	ltmp.size = sizeof(idt);
	ltmp.addr = &idt[0];
	asm volatile("lidt [eax]" : : "a"(&ltmp) : "memory");
	
	int i;
	for(i = 0; i < 32; i++){
		if(i == 0){//super special case, make another special function
			table(i, divideByZeroInterrupt);
		}
		else if(i == 3){//debug, make another special function
			table(i, debugInterrupt);
		}
		else if(i == 6){
			table(i, badOpcodeInterrupt);
		}
		else if(i == 13){
			table(i , protectionFaultInterrupt);
		}
		else if(i == 14){
			table(i, pageFaultInterrupt);
		}
		else if(i == 8 || i == 10 || i == 11 || i == 12 || i == 17){
			table(i, unknownInterruptWithCode);
		}
		else{//i is not a pink, and not a super special case
			table(i, unknownInterrupt);
		}
	}
}

int exec(const char * filename){
	kprintf("We're inside our function\n");
	//Load binary to RAM at 0x400000
	int f = file_open(filename, 0);
	kprintf("f: %d", f);
	if(f < 0){
		return ENIVAL;
	}
	kprintf("We've opened the file\n");
	int i = 0;
	int res = file_read(f, (void *)0x400000, 100);
	i += res;
	while(res != 0){
		res = file_read(f, (void *)0x400000 + i, 100);
		i += res;
	}

	file_close(f);

	kprintf("right before the assembly\n");

	asm volatile(
		"mov ax,27\n"
		"mov ds,ax\n"
		"mov es,ax\n"
		"mov fs,ax\n"
		"mov gs,ax\n"
		"push 27\n"
		"push 0x800000\n"
		"pushf\n"	//push eflags register
		"push 35\n"
		"push 0x400000\n"
		"iret"
		::: "eax","memory" );
	kprintf("We should never get here!\n");
	haltForever();	

	return SUCCESS;
}