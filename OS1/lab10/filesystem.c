#include "filesystem.h"
#include "direntry.h"
#include "disk.h"
#include "kprintf.h"
#include "errno.h"
#include "console.h"

unsigned get_file_inode(unsigned dir_inode, const char * filename){
	kprintf("%s\n", filename);
	kprintf("%d\n", dir_inode);
	//return 0 if there is no such file
	int block = 4096;
	//int d = 0;
	static struct Inode ino;
	disk_read_inode(dir_inode, &ino);

	kprintf("ino: %d\n", ino.size);

	static char dEntBuff[4096];
	kprintf("ino.direct[0] : %d\n", ino.direct[0]);
	disk_read_block(ino.direct[0], dEntBuff);
	struct DirEntry * dEnt = (struct DirEntry *)dEntBuff;

	kprintf("%d\n", dEnt->rec_len);
	while(dEnt->rec_len != 0){
		kprintf("%d\n", dEnt->name_len);
		int pass = 1;
		block -= dEnt->rec_len;
		// if(block <= 0){
		// 	d++;
		// 	//dIndex = 0;
		// 	block = 4096;
		// 	disk_read_block(ino.direct[d], dEntBuff);
		// }
		if (ino.mode >> 12 == 8) {
			for(int i = 0; i < dEnt->name_len; i++) {
			//kprintf("here\n");
				if(dEnt->name[i] != filename[i]) {
					kprintf("%c  %c\n", dEnt->name[i], filename[i]);
					pass = 0;
					break;
				}
			}

			if(pass == 1 && dEnt->name_len > 0){
				return dEnt->inode;	
			}

		}

		dEnt = (struct DirEntry *)(dEntBuff + dEnt->rec_len);
	}
	return 0;
}

int file_open(const char * fname, int flags){
	kprintf("%s\n", fname);
	int idx = 0;


	kprintf("%d\n", fileTbl[idx].inUse);
	fileTbl[idx].inUse = 1;
	while(fileTbl[idx].inUse == 1){
		idx++;

		kprintf("%d %d\n", idx, FILEMAX);
		if(idx >= FILEMAX){
			return -EMFILE;
		}
	}

	kprintf("%d\n", idx);
	int ino_num = get_file_inode(2, fname);

	kprintf("ino_num: %d\n", ino_num);
	if(ino_num == 0){
		return -ENOENT; // no such file in directory	
	}

	// I need to, have a file descriptor that is empty, a number that doesn't exist
	//loop through file table, and find the first available spot
	//3files open, 4 isn't being used, so we set 4
	//kprintf("ino_num I'm trying to open: %d\n", ino_num);
	disk_read_inode(ino_num, &fileTbl[idx].ino);

	fileTbl[idx].inUse = 1;
	fileTbl[idx].offset = 0;
	return idx;
}

int file_close(int fd){
	//returns 0 for success, nonzero for errors
	if(fileTbl[fd].inUse != 1){
		return EMFILE;
	}

	fileTbl[fd].inUse = 0;

	return SUCCESS;
}

int file_read(int fd, void * buffer, int count) {
	if (fd >= FILEMAX || fd < 0) {
		return ENOENT;
	}

	struct fileSys * fp = &fileTbl[fd];

	if (fp->inUse == 0) {
		return EMFILE;
	}

	if (count <= 0 || fp->offset >= fp->ino.size) {
		return 0;
	}

	static char buff[4096];
	static unsigned int U[1024];

	int bi = fp->offset / 4096;

	if (bi < 12) {
		disk_read_block(fp->ino.direct[bi], buff);
	} else {
		bi -= 12;
		if (bi < 1024) {
			disk_read_block(fp->ino.indirect, (void *)U);
			disk_read_block(U[bi], buff);
		}
		else {
			bi -= 1024;
			if (bi < 1048576) {
				disk_read_block(fp->ino.doubleindirect, (void *)U);
				disk_read_block(U[bi >> 10], (void *)U);
				disk_read_block(U[bi % 1024], buff);
			}
		}
	}

	int bo = fp->offset % 4096;

	int final_count = count;
	int buffer_size = 4096 - bo;
	int remaining_file_size = fp->ino.size - fp->offset;

	if (buffer_size < final_count ) {
		final_count = buffer_size;
	}
	if (remaining_file_size < final_count) {
		final_count = remaining_file_size;
	}

	kmemcpy(buffer, buff + bo, final_count);
	fp->offset += final_count;

	return final_count;
}



void list_directory(int dir_inode, int indent){
	static struct Inode * ino;
	//static char bigBoiBuff[4096];
	static char buffGuy[4096];
	int d = 0;
	int i = 0;
	int dIndex = 0;
	int sz = 0;
	int block = 4096;
	//int sz = 0;
	disk_read_inode(4, ino);
	static struct DirEntry * dEnt;
	// disk_read_block(4, (void *)bigBoiBuff);
	// ino = (struct Inode *)bigBoiBuff;
	disk_read_block(ino[dir_inode-1].direct[d], buffGuy);
	dEnt = (struct DirEntry *)(buffGuy + dIndex);
	
	//while i can print, and don't run out of space
	while(dEnt->rec_len != 0 && sz < ino[dir_inode-1].size){
		//another reset
		dEnt = (struct DirEntry *)(buffGuy + dIndex);

		//printing out my file depth
		for(i = 0; i < indent; i ++){
			kprintf("_");
		}
		
		kprintf("<%d>", dEnt->inode);
		kprintf("%.*s\n", dEnt->name_len, dEnt->name);
		//kprintf(" Name len: %d", dEnt->name_len);

		//keeping track of what dir entry I am at
		dIndex += dEnt->rec_len;
		sz += dEnt->rec_len;
		block -= dEnt->rec_len;
		if(block <= 0){
			d++;
			dIndex = 0;
			block = 4096;
			disk_read_block(ino[dir_inode-1].direct[d], buffGuy);
		}
		//int mode = ino[dir_inode-1].mode >> 12;
		//kprintf("mode: %d\n", mode);

		if(ino[dir_inode-1].mode >> 12 == 4 && dEnt->name_len > 2 ){
			list_directory(dEnt->inode, indent + 4);
			disk_read_inode(4, ino);
			// disk_read_block(4, (void *)bigBoiBuff);
			// ino = (struct Inode *)bigBoiBuff;
			disk_read_block(ino[dir_inode-1].direct[d], buffGuy);
			dEnt = (struct DirEntry *)(buffGuy + dIndex);
		} 		
	}
}