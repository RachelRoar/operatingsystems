#pragma once

#include "disk.h"

struct fileSys{
	int offset;
	int inUse;
	struct Inode ino;
};

#define FILEMAX 16
struct fileSys fileTbl[FILEMAX];

void list_directory(int dir_inode, int indent);
unsigned get_file_inode(unsigned dir_inode, const char * filename);
int file_open(const char * fname, int flags);
int file_read(int fd, void * buffer, int count);
int file_close(int id);
