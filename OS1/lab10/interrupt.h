#pragma once

#pragma pack(push, 1)
struct IDTentry{
	unsigned short addrLow; //address of interupt handler
	unsigned short selector; //code segment
	unsigned char zero;	//must be zero
	unsigned char flags;	//0x8e for interrupt handler
	unsigned short addrHigh;
};
#pragma pack(pop)


#pragma pack(push, 1) 
struct GDTEntry{
	unsigned short limitLow;   // how big is the segment? (split in two parts) set to 0xffff
	unsigned char base0, base1, base2;
	unsigned short flagsAndLimitHigh;	//0xcf9a code segment 0xcf92 data segment
	unsigned char base3;
};
#pragma pack(pop)


#pragma pack(push, 1)
struct LGDT{
	unsigned short size;  //size of our tables
	struct GDTEntry * addr;   //table address
};
#pragma pack(pop)


#pragma pack(push, 1)
struct InterruptFrame{
	unsigned eip;
	unsigned cs;
	unsigned eflags;
	unsigned esp; //only used when undergoing, undergoing what?
	unsigned ss; //a ring transition
};
#pragma pack(pop)


#pragma pack(push, 1)
struct LIDT{
	unsigned short size;
	struct IDTentry * addr;
};
#pragma pack(pop)


void interrupt_init();
void table(int i, void * func);
int exec(const char * filename);