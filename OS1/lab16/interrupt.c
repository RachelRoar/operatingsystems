#include "interrupt.h"
#include "kprintf.h"


struct GDTEntry gdt[] = {
	{0, 0, 0, 0, 0, 0 },    //zeros
	{ 0xffff, 0,0,0, 0xcf92, 0 },	//data
	{ 0xffff, 0,0,0, 0xcf9a, 0 }	//code
};


void haltForever(void){
	while(1){
		asm volatile("hlt" ::: "memory");
	}
}

__attribute__((__interrupt__))
void unknownInterrupt(struct InterruptFrame * fr){
	kprintf("\nFatal exception at eip=%x\n",fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void unknownInterruptWithCode(struct InterruptFrame * fr, unsigned code){
	kprintf("Fatal exception: Code=%x eip=%x\n",code,fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void divideByZeroInterrupt(struct InterruptFrame * fr){
	kprintf("\nDivide by Zero at eip=%x\n",fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void debugInterrupt(struct InterruptFrame * fr){
	kprintf("Debug exception: eip=%x\n",fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void protectionFaultInterrupt(struct InterruptFrame * fr, unsigned code){
	kprintf("Protection Fault exception: Code = %x eip=%x\n",code,fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void pageFaultInterrupt(struct InterruptFrame * fr, unsigned code){
	kprintf("Page Fault exception: Code = %x eip=%x\n",code,fr->eip);
	haltForever();
}

__attribute__((__interrupt__))
void badOpcodeInterrupt(struct InterruptFrame * fr, unsigned code){
	kprintf("Bad Opcode exception: Code=%x eip=%x\n",code,fr->eip);
	haltForever();
}

struct IDTentry idt[32];

void table(int i, void * func){
	unsigned x = (unsigned)func;
	idt[i].addrLow = x&0xffff;
	idt[i].selector = 2 << 3;
	idt[i].zero = 0;
	idt[i].flags = 0x8e;
	idt[i].addrHigh = x >> 16;
}

//interrupt initialization
void interrupt_init(){
	struct LGDT lgdt;
	lgdt.size = sizeof(gdt);
	lgdt.addr = &gdt[0];
	asm volatile( "lgdt [eax]" : : "a"(&lgdt) : "memory" );
	
	struct LIDT tmp;
	tmp.size = sizeof(idt);
	tmp.addr = &idt[0];
	asm volatile("lidt [eax]" : : "a"(&tmp) : "memory");
	
	int i;
	for(i = 0; i < 32; i++){
		if(i == 0){//super special case, make another special function
			table(i, divideByZeroInterrupt);
		}
		else if(i == 3){//debug, make another special function
			table(i, debugInterrupt);
		}
		else if(i == 6){
			table(i, badOpcodeInterrupt);
		}
		else if(i == 13){
			table(i , protectionFaultInterrupt);
		}
		else if(i == 14){
			table(i, pageFaultInterrupt);
		}
		else if(i == 8 || i == 10 || i == 11 || i == 12 || i == 17){
			table(i, unknownInterruptWithCode);
		}
		else{//i is not a pink, and not a super special case
			table(i, unknownInterrupt);
		}
	}
}