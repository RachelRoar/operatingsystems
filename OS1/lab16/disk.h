#pragma once


#pragma pack(push,1)
struct Inode { 
	 unsigned short mode; 
	 unsigned short uid; 
	 unsigned size; 
	 unsigned atime; 
	 unsigned ctime; 
	 unsigned mtime; 
	 unsigned dtime;
	 unsigned short gid;
	 unsigned short links;
	 unsigned blocks;
	 unsigned flags;
	 unsigned osd1;
	 unsigned direct[12];
	 unsigned indirect;
	 unsigned doubleindirect;
	 unsigned tripleindirect;
	 unsigned generation;
	 unsigned fileacl;
	 unsigned diracl;
	 unsigned osd2;
	 char reserved[12];
};
#pragma pack(pop)



void disk_read_sector(unsigned sector, void * datablock);
void disk_write_sector(unsigned sector, const void * datablock);
void outb(unsigned short port, unsigned char value);
void outw(unsigned short port, unsigned short value);
unsigned char inb(unsigned short port);
unsigned short inw(unsigned short port);
int isBusy();
void disk_read_block(unsigned block, const void * datablock);
void disk_read_inode(unsigned num, struct Inode * ino);