#ifndef STDARG_H
#define STDARG_H

typedef struct _va_list{
	char * p;
}va_list;

#define va_start(V, L) _va_start(&V, &L, sizeof(L)) // V=argtype of va_list 
													// L=last nonvariadic argument
#define va_arg(V, T) (*(T*)(_va_arg(&V, sizeof(T))))

static void _va_start(va_list * V, void * L, int size){
	V->p = ((char *)L + size);
}

char * _va_arg(va_list * V, unsigned size){
	char * tmp = V->p;
	V->p += size;
	return tmp;
}

static void va_end(va_list V) {}

#endif