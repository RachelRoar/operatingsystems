#include "filesystem.h"
#include "inode.h"
#include "direntry.h"
#include "disk.h"
#include "kprintf.h"
#include "errno.h"



int file_open(const char * fname, int flags){

	int idx = 0;
	//returns descriptor or error code. Errors are negative

	int i;
	for(i = 0; i < FILEMAX; i++){
		if(fileTbl[idx]->inUse == 1){
			idx++;
		}

		if(idx >= FILEMAX){
			return EMFILE;
		}
	}

	int ino_num = get_file_inode(2, fname);
	if(ino_num == 0){
		return ENOENT; // no such file in directory
	}
	disk_read_inode(ino_num, &fileTbl[idx]->ino);

	fileTbl[idx]->inUse = 1;
	fileTbl[idx]->offset = 0;

	return idx;
}

int file_close(int fd){
	//returns 0 for success, nonzero for errors
	if(fd >= FILEMAX || fd < 0) {
		return ENIVAL;
	}

	if(fileTbl[fd]->inUse == 0){
		return EMFILE;
	}

	fileTbl[fd]->inUse = 0;
	return SUCCESS;
}

unsigned get_file_inode(unsigned dir_inode, const char * filename){
	//return 0 if there is no such file
	struct Inode * ino;
	disk_read_inode(dir_inode, ino);

	static char dEntBuff[4096];
	disk_read_block(ino[dir_inode-1].direct[0], dEntBuff);
	struct DirEntry * dEnt = (struct DirEntry *)dEntBuff;

	while(dEnt->rec_len != 0){
		disk_read_inode(dEnt->inode, ino);

		int i;
		for(i = 0; i < dEnt->name_len; i++){
			if(dEnt->name[i] != filename[i]){
				return 0;
			}
		}
		return dEnt->inode;
	}
	return 0;
}

void list_directory(int dir_inode, int indent){
	static struct Inode * ino;
	static char bigBoiBuff[4096];
	static char buffGuy[4096];
	int d = 0;
	int i = 0;
	int dIndex = 0;
	int sz = 0;
	int block = 4096;
	//int sz = 0;
	static struct DirEntry * dEnt;
	disk_read_block(4, (void *)bigBoiBuff);
	ino = (struct Inode *)bigBoiBuff;
	disk_read_block(ino[dir_inode-1].direct[d], buffGuy);
	dEnt = (struct DirEntry *)(buffGuy + dIndex);
	
	//while i can print, and don't run out of space
	while(dEnt->rec_len != 0 && sz < ino[dir_inode-1].size){
		//another reset
		dEnt = (struct DirEntry *)(buffGuy + dIndex);

		//printing out my file depth
		for(i = 0; i < indent; i ++){
			kprintf("_");
		}
		
		kprintf("<%d>", dEnt->inode);
		kprintf("%.*s\n", dEnt->name_len, dEnt->name);
		//kprintf(" Name len: %d", dEnt->name_len);

		//keeping track of what dir entry I am at
		dIndex += dEnt->rec_len;
		sz += dEnt->rec_len;
		block -= dEnt->rec_len;
		if(block <= 0){
			d++;
			dIndex = 0;
			block = 4096;
			disk_read_block(ino[dir_inode-1].direct[d], buffGuy);
		}
		//int mode = ino[dir_inode-1].mode >> 12;
		//kprintf("mode: %d\n", mode);

		if(ino[dir_inode-1].mode >> 12 == 4 && dEnt->name_len > 2 ){
			//list_directory(dEnt->inode, indent + 4);
			disk_read_block(4, (void *)bigBoiBuff);
			ino = (struct Inode *)bigBoiBuff;
			disk_read_block(ino[dir_inode-1].direct[d], buffGuy);
			dEnt = (struct DirEntry *)(buffGuy + dIndex);
		} 		
	}
}