#pragma once

#include "disk.h"

struct fileSys{
	int offset;
	int inUse;
	struct Inode ino;
};

#define FILEMAX 16
struct fileSys * fileTbl[FILEMAX];


void list_directory(int dir_inode, int indent);
int file_open(const char * fname, int flags);
int file_close(int id);
unsigned get_file_inode(unsigned dir_inode, const char * filename);