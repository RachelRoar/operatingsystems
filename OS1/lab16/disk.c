#include "kprintf.h"
#include "disk.h"
#include "console.h"
#include "superblock.h"
//#include "inode.h"

//writing
void outb(unsigned short port, unsigned char value){
	asm volatile("out dx, al"
		:
		: "a"(value), "d"(port)
		: "memory"
	);
}

void outw(unsigned short port, unsigned short value){
	asm volatile("out dx, ax"
		:
		: "a"(value), "d"(port)
		: "memory"
	);
}
//reading
unsigned char inb(unsigned short port){
	unsigned value;
	asm volatile("in al, dx" : "=a"(value): "d"(port));
	return (unsigned char) value;
}

unsigned short inw(unsigned short port){
	unsigned value;
	asm volatile("in ax, dx" : "=a"(value): "d"(port) );
	return (unsigned short) value;
}

int isBusy(){
	return inb(0x1f7) & 0x80;
}

void disk_read_sector(unsigned sector, void * datablock)
{
	unsigned short * v = (unsigned short *)datablock;
	while(isBusy()) 
		;
	outb(0x3f6, 2); //turning off interupts
	outb(0x1f2, 1);
	outb(0x1f6, 0xe0 | (sector >> 24));
	outb(0x1f3, 0xFF & sector);
	outb(0x1f4, 0xFF  & (sector >> 8));
	outb(0x1f5, 0xFF & (sector >> 16));
	outb(0x1f7, 0x20);
	while(isBusy())
		;
	unsigned char statRegister = inb(0x1f7);
	if(statRegister & 0x01 || statRegister & 0x20 ){
		kprintf("You are not welcome to Valhalla for reading");
		return;
		//outb(0x1f7, 0xe7);
	} else if (statRegister & 0x08) {
		int i;
		for(i = 0; i < 256; i++){
			unsigned short d = inw(0x1f0);
			v[i] = d;
		}
	}
	

}

void disk_write_sector(unsigned sector, const void * datablock)
{
	const short * p = (const short *)datablock;
	while(isBusy()) 
		;
	outb(0x3f6, 2); //turning off interupts
	outb(0x1f2, 1);
	outb(0x1f6, 0xe0 | (sector >> 24));
	outb(0x1f3, 0xFF & sector);
	outb(0x1f4, 0xFF & (sector >> 8));
	outb(0x1f5, 0xFF & (sector >> 16));
	outb(0x1f7, 0x30);
	while(isBusy())
		;
	if(inb(0x1f7) & 0x01 || inb(0x1f7) & 0x20 ){
		kprintf("You are not welcome to Valhalla for writing");
		return;
		//outb(0x1f7, 0xe7);
	} else {
		int i;
		for(i = 0; i < 256; i++){
			outw(0x1f0, * p);
			p++;
		}
	}
}

//ToDo: Fix This
void disk_read_block(unsigned block, const void * datablock){
	unsigned char * d = (unsigned char * )datablock;
	//char buff[4096];
	int i;
	for(i = 0; i < 8; i++){
		disk_read_sector((block * 8) + i, d);
		d += 512;
		//disk_read_sector((block * 8) + i, buff + (i * 512));
	}
	//kmemcpy((void *)datablock, buff, 4096);
}

void disk_read_inode(unsigned ino_num, struct Inode * ino){

	static char inoblockbuff[4096];
	//int inoGrpNum;
	//int inoBlock;
	int inoFromStart;
	//int bytesFromStart;
	//int blocksFromStart;
	//int desiredBlock;
	int inoPerBlock;
	int inoToSkip;
	static char sprblockbuff[4096];

	// disk_read_block(4, (void *)inoblockbuff);
	// ino = (struct Inode *)inoblockbuff;
	
	disk_read_block(0, (void *)sprblockbuff);
	kmemcpy(&sprBlock, (sprblockbuff + 1024), sizeof(sprBlock));

	//inoGrpNum = ino_num - 1 / sprBlock.inodes_per_group;
	//inoBlock = sprBlock.blocks_per_group * inoGrpNum + 4; //super block, descript block tbl, block bitmap, inode bitmap

	inoFromStart = (ino_num - 1) % sprBlock.inodes_per_group; 

	//bytesFromStart = inoFromStart * sizeof(struct Inode);

	//blocksFromStart = bytesFromStart / 4096; //block size = 4096

	//desiredBlock = inoFromStart + bytesFromStart;

	inoPerBlock = 4096 / sizeof(struct Inode);

	inoToSkip = inoFromStart % inoPerBlock;
	disk_read_block(4, (void *)inoblockbuff);
	ino = (struct Inode *)inoblockbuff;
	kmemcpy(ino, inoblockbuff + inoToSkip * sizeof(ino), sizeof(ino));
	//disk_read_block(inoBlock, (void *)ino);
}