#include "util.h"
#include "console.h"
#include "font.h"
#include "disk.h"
#include "interrupt.h"
#include "kprintf.h"

void sweet();

void kmain(struct MultibootInfo * mbi){
	console_init(mbi);
	interrupt_init();

	sweet();

	//do nothing below this shit
	while(1){
		asm volatile("hlt");
	}
}

void clearBss(char * bssStart, char * bssEnd)
{
	while (bssStart != bssEnd)
	{
		*bssStart = 0;
		bssStart ++;
	}
}