#include "filesystem.h"
#include "inode.h"
#include "direntry.h"
#include "disk.h"
#include "kprintf.h"

//initialization


void list_directory(int dir_inode, int indent){
	static struct Inode * ino;
	//static char bigBoiBuff[4096];
	static char buffGuy[4096];
	int d = 0;
	int i = 0;
	int dIndex = 0;
	int sz = 0;
	int block = 4096;
	//int sz = 0;
	disk_read_inode(4, ino);
	static struct DirEntry * dEnt;
	// disk_read_block(4, (void *)bigBoiBuff);
	// ino = (struct Inode *)bigBoiBuff;
	disk_read_block(ino[dir_inode-1].direct[d], buffGuy);
	dEnt = (struct DirEntry *)(buffGuy + dIndex);
	
	//while i can print, and don't run out of space
	while(dEnt->rec_len != 0 && sz < ino[dir_inode-1].size){
		//another reset
		dEnt = (struct DirEntry *)(buffGuy + dIndex);

		//printing out my file depth
		for(i = 0; i < indent; i ++){
			kprintf("_");
		}
		
		kprintf("<%d>", dEnt->inode);
		kprintf("%.*s\n", dEnt->name_len, dEnt->name);
		//kprintf(" Name len: %d", dEnt->name_len);

		//keeping track of what dir entry I am at
		dIndex += dEnt->rec_len;
		sz += dEnt->rec_len;
		block -= dEnt->rec_len;
		if(block <= 0){
			d++;
			dIndex = 0;
			block = 4096;
			disk_read_block(ino[dir_inode-1].direct[d], buffGuy);
		}
		//int mode = ino[dir_inode-1].mode >> 12;
		//kprintf("mode: %d\n", mode);

		if(ino[dir_inode-1].mode >> 12 == 4 && dEnt->name_len > 2 ){
			list_directory(dEnt->inode, indent + 4);
			disk_read_inode(4, ino);
			// disk_read_block(4, (void *)bigBoiBuff);
			// ino = (struct Inode *)bigBoiBuff;
			disk_read_block(ino[dir_inode-1].direct[d], buffGuy);
			dEnt = (struct DirEntry *)(buffGuy + dIndex);
		} 		
	}
}