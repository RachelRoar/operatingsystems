#pragma once
#include "util.h"


void console_init(struct MultibootInfo * mbi);
void set_pixel(int x, int y, int r, int g, int b);
void drawR(int Start, int Stop, int intermediate);
void console_draw_char(int x, int y, char ch);
void console_draw_string(int x, int y, const char * s);
//For lab 5, writing lines
void kmemset(void * p, char c, int n);
void console_putc(char c);