//out
#include "kprintf.h"
#include "disk.h"

//writing
void outb(unsigned short port, unsigned char value){
	asm volatile("out dx, al"
		:
		: "a"(value), "d"(port)
		: "memory"
	);
}

void outw(unsigned short port, unsigned short value){
	asm volatile("out dx, ax"
		:
		: "a"(value), "d"(port)
		: "memory"
	);
}
//reading
unsigned char inb(unsigned short port){
	unsigned value;
	asm volatile("in al, dx" : "=a"(value): "d"(port));
	return (unsigned char) value;
}

unsigned short inw(unsigned short port){
	unsigned value;
	asm volatile("in ax, dx" : "=a"(value): "d"(port) );
	return (unsigned short) value;
}

int isBusy(){
	return inb(0x1f7) & 0x80;
}

// while(isBusy()){
// 	;
// }

void disk_read_sector(unsigned sector, void * datablock)
{
	unsigned short * v = (unsigned short *)datablock;
	while(isBusy()) 
		;
	outb(0x3f6, 2); //turning off interupts
	outb(0x1f2, 1);
	outb(0x1f6, 0xe0 | (sector >> 24));
	outb(0x1f3, 0xFF & sector);
	outb(0x1f4, 0xFF  & (sector >> 8));
	outb(0x1f5, 0xFF & (sector >> 16));
	outb(0x1f7, 0x20);
	while(isBusy())
		;
	if(inb(0x1f7) & 0x01 || inb(0x1f7) & 0x20 ){
		kprintf("You are not welcome to Valhalla for reading");
		return;
		//outb(0x1f7, 0xe7);
	} else {
		int i;
		for(i = 0; i < 256; i++){
			unsigned short d = inw(0x1f0);
			v[i] = d;
		}
	}
}

void disk_write_sector(unsigned sector, const void * datablock)
{
	const short * p = (const short *)datablock;
	while(isBusy()) 
		;
	outb(0x3f6, 2); //turning off interupts
	outb(0x1f2, 1);
	outb(0x1f6, 0xe0 | (sector >> 24));
	outb(0x1f3, 0xFF & sector);
	outb(0x1f4, 0xFF & (sector >> 8));
	outb(0x1f5, 0xFF & (sector >> 16));
	outb(0x1f7, 0x30);
	while(isBusy())
		;
	if(inb(0x1f7) & 0x01 || inb(0x1f7) & 0x20 ){
		kprintf("You are not welcome to Valhalla for writing");
		return;
		//outb(0x1f7, 0xe7);
	} else {
		int i;
		for(i = 0; i < 256; i++){
			outw(0x1f0, * p);
			p++;
		}

	}
}

