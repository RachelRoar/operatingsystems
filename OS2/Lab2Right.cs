﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; //This is where I can read URL's
using System.Threading;
using System.IO;


class Threads
{
    static public void WebReader(List<string> urlList)
    {
        
            string fileWithNoName = "index.html";
            System.Net.WebClient w = new WebClient();
            //Console.WriteLine(urlName);
            for (int i = 0; i < urlList.Count; i++)
            {
                //if (urlList[i] == "")
                //{
                //    break;
                //}
                new System.Threading.Thread(() =>
                {
                    try {
                        System.Uri url = new Uri(urlList[i]);
                        string s = url.AbsolutePath;
                        string[] tmp = s.Split('/');
                        if (tmp[tmp.Length - 1].Equals(""))
                        {
                            //Console.WriteLine(urlList[i] + ": in)
                            w.DownloadFile(url, fileWithNoName);
                        }
                        else
                        {
                            string fileName = tmp[tmp.Length - 1];
                            w.DownloadFile(url, fileName);
                        }
                    } catch(Exception e) {

                    }
                    
                }).Start();
        }
        
    }

    static void OpenFile(string filename)
    {
        //Console.WriteLine(filename);
        List<string> websites = new List<string>();
        string url = "";

        using (var F = File.OpenText(filename))
        {
            while (true)
            {
                var ch = F.Read();
                if (ch == -1)
                    break;
                char c = (char)ch;
                //char newLine = 
                if (c.Equals('\r') || c.Equals('\n')) {                    websites.Add(url);
                    url = "";
                    //break;
                } else {
                    url += c;
                }

            }
            websites.Add(url);

        }
        //Console.WriteLine(words);
        for (int i = 0; i < websites.Count; i++) {
            if(websites[i].Equals("")) {
                websites.Remove(websites[i]);
            }
        }
        WebReader(websites);
    }
    static void Main(string[] args)
    {
        string alex = args[0];
        //string url = "http://forestry.ohiodnr.gov/Portals/forestry/PDFs/SF/shawnee_backpack.pdf";
        var T = new Thread( () => OpenFile(alex));
        T.Start();
    }
}

